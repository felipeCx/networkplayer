﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class CustomerSpawner : NetworkBehaviour {
	public GameObject customerPrefab;
	public Transform spawnPoint;
	public List<DeliveryTable> deliveryTable;

	public StageOrderConfig stageOrderConfig;

	public Transform ExitTransform;

	[SerializeField] private int customersToSpawn = 1;
	

	public void StartEssaCoisa()
	{
		StartCoroutine(SpawnCoroutine());
	}

	private IEnumerator SpawnCoroutine()
	{
		int i = 0;
		var wait = new WaitForEndOfFrame();

		var orderCount = stageOrderConfig.listConfig.Count;

		yield return new WaitForSeconds(1.0f);

		while(i < orderCount)
		{			
			var destination = deliveryTable.Where(d => !d.IsBusy).FirstOrDefault();

			if(destination != null)
			{
				var customerInstance = Instantiate(customerPrefab, spawnPoint.position, spawnPoint.rotation);

				var customerController = customerInstance.GetComponent<CustomerController>();
				customerController.ExitTransform = ExitTransform;

				NetworkServer.Spawn(customerInstance);

				customerController.SetClientOrderId(stageOrderConfig.listConfig[i].ingredientId);
				
				customerController.MoveTo(destination.StandPoint);
				
				destination.CurrentCustomer = customerInstance;
				destination.IsBusy = true;

				i ++;

				yield return new WaitForSeconds(5.0f);
			}
			else
			{
				yield return wait;
			}
		}
	}
	
	private void SetOrderToCustomer(CustomerController customer, int id)
	{
		
	}

	public int GetCustomersAmount()
	{
		return stageOrderConfig.listConfig.Count;
	}
}
