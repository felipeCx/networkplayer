﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CustomerAnimatorController : NetworkBehaviour {
	[SerializeField]
	private Animator animator;
	[SerializeField]
	private NetworkAnimator networkAnimator;

	public void Awake()
	{
		
		networkAnimator.SetParameterAutoSend(0, true);
		
		networkAnimator.SetParameterAutoSend(2, true);
	}

	[ClientRpc]
	public void RpcSetSayHiAnimation()
	{
		animator.SetTrigger("SayHi");
	}
	
	[ClientRpc]
	public void RpcSetSpeedParam(float speed)
	{
		Debug.Log("Seting Speed");
		animator.SetFloat("Speed", speed);
	}
}
