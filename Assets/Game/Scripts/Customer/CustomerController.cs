﻿using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class CustomerController : NetworkBehaviour {

	private OrderConfig orderConfig;
	private int orderId;
	private Tween movementTween;

	[HideInInspector]
	public Transform ExitTransform;

	[HideInInspector]
	public DeliveryTable currentDelivery;

	public GameObject OrderCanvas;
	public Image OrderIcon;

	[SerializeField]
	CustomerAnimatorController animatorController;

	private Coroutine waitCoroutine;

	private bool isServed;

	[SyncVar]
	private int ScoreToGive = 100;

	void Start()
	{
		StartCoroutine(ScoreToGiveCoroutine());
	}

	private IEnumerator ScoreToGiveCoroutine()
	{
		while(ScoreToGive >= 5 || isServed)
		{
			ScoreToGive = Mathf.RoundToInt(ScoreToGive * 0.8f);
			
			yield return new WaitForSeconds(7.0f);
		}
	}

	public void ShouldConsume()
	{
		var currentDeliveryIngredient = currentDelivery.GetCurrentObjectIngredientController();
		
		if(IsRightOrder(currentDeliveryIngredient))
		{
			Consume();
			isServed = true;
		}
		else
		{
			animatorController.RpcSetSayHiAnimation();
		}
	}

	private void Consume()
	{
		MatchManager.Instance.UpdateScore(ScoreToGive);
		currentDelivery.CmdConsumeDelivery();
		Exit();

		MatchManager.Instance.FinalizeCustomer();
	}	

	public void MoveTo(Transform to)
	{
		currentDelivery = to.GetComponentInParent<DeliveryTable>();
		currentDelivery.IsBusy = true;

		animatorController.RpcSetSpeedParam(1.0f);

		this.transform.LookAt(to.position);
		movementTween = this.transform.DOMove(to.position, 2.0f);

		movementTween.OnComplete(StartWaitCoroutine);
		
		movementTween.Play();
	}

	public void Exit()
	{
		this.transform.LookAt(ExitTransform.position);
		animatorController.RpcSetSpeedParam(1.0f);
		
		movementTween = this.transform.DOMove(ExitTransform.position, 2.0f);
		movementTween.OnComplete(AutoDestroy);
		movementTween.Play();
		
		RpcHideOrderCanvas();

		currentDelivery.IsBusy = false;
	}

	public void SetClientOrderId(int id)
	{
		if(isClient)
			CmdSetOrderId(id);
	}

	[Command]
	public void CmdSetOrderId(int id)
	{
		Debug.Log("DANDO COMMAND");
		this.orderId = id;
		
		RpcSetOrder(id);
	}

	[ClientRpc]
	public void RpcSetOrder(int id)
	{
		this.orderId = id;

		var path = "Configs/IngredientConfigs/Ingredient0" + id;
		
		orderConfig = Resources.Load<OrderConfig>(path);
	}

	private void StartWaitCoroutine()
	{
		animatorController.RpcSetSpeedParam(0.0f);
		animatorController.RpcSetSayHiAnimation();

		RpcShowOrderCanvas();
		waitCoroutine = StartCoroutine(CouroutineWaitForDelivery());
	}

	public IEnumerator CouroutineWaitForDelivery()
	{
		while (currentDelivery.CurrentObject == null)
		{
			yield return new WaitForEndOfFrame();
		}

		var currentDeliveryIngredient = currentDelivery.GetCurrentObjectIngredientController();
		
		if(IsRightOrder(currentDeliveryIngredient))
		{
			currentDelivery.CmdConsumeDelivery();
			Exit();
		}
		else
		{
			StopCoroutine(waitCoroutine);
			animatorController.RpcSetSayHiAnimation();
		}
	}

	private bool IsRightOrder(IngredientController ingredient)
	{

		if (ingredient == null || orderConfig == null)
		{
			return false;
		}
		
		Debug.Log(ingredient.IngredientType + " " + orderConfig.ingredient);

		return ingredient.IngredientType == orderConfig.ingredient
			&& ingredient.IngredientStateType == IngredientStateEnum.DONE;
	}

	[ClientRpc]
	private void RpcShowOrderCanvas()
	{
		if (orderConfig == null)
		{
			return;
		}
		OrderIcon.sprite = orderConfig.icon;
		var orderTween = OrderCanvas.transform.DOScale(Vector3.one, 0.5f);
		orderTween.SetEase(Ease.InQuad);
		orderTween.Play();
	}

	[ClientRpc]
	private void RpcHideOrderCanvas()
	{
		var orderTween = OrderCanvas.transform.DOScale(Vector3.zero, 0.5f);
		orderTween.SetEase(Ease.InQuad);
		orderTween.Play();
	}

	private void AutoDestroy()
	{
		Destroy(this.gameObject);
	}
}
