﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class TimerController : MonoBehaviour
{
    public GameObject TimerCanvas;
    public Image FillImage;

    public event Action OnFinishTimer;

    void Start()
    {
        StartTime();
    }

    public void StartTime()
    {
        var timerTween = DOTween.To(() => FillImage.fillAmount, x => FillImage.fillAmount = x, 0.0f, 5.0f);

        timerTween.Play();

        timerTween.OnComplete(OnFinishTimerEvent);
    }

    private void OnFinishTimerEvent()
    {
        if (OnFinishTimer != null)
        {
            OnFinishTimer.Invoke();
        }
    }
}