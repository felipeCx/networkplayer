﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverPopup : NetworkBehaviour {

	public Text txtScore;
	public Text txtRecord;
	public Button QuitButton;

	private Tween visibilityTween;

	[SerializeField]
	private Ease ease = Ease.InOutQuad;

	// Use this for initialization
	void Awake () {
		QuitButton.onClick.AddListener(ReloadScene);	
	}

	void OnDestroy()
	{
		QuitButton.onClick.RemoveListener(ReloadScene);
	}
	
	private void ReloadScene()
	{
		SceneManager.LoadScene(0);
	}

	public void FillPopup(int score, int record)
	{
		RpcFillPopup(score, record);
	}

	[ClientRpc]
	public void RpcFillPopup(int score, int record)
	{
		txtScore.text = score.ToString();
		txtRecord.text = record.ToString();
	}
}
