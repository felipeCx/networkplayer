﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour {
	Transform cameraTransform;
	// Use this for initialization
	void Awake () {
		cameraTransform = Camera.main.transform;
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.LookAt(cameraTransform);
	}
}
