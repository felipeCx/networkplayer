﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerAnimatorController : NetworkBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private NetworkAnimator networkAnimator;
    private int triggerHash;

    void Awake()
    {
        triggerHash = Animator.StringToHash("Speed");
    }

    public void CmdSetRunAnimationParameter(float value)
    {
        animator.SetFloat(triggerHash, value);
        networkAnimator.SetParameterAutoSend(0, true);
    }

    [ClientRpc]
    public void RpcSetHoldAnimation(bool state)
    {
        animator.SetBool("IsHolding", state);
        networkAnimator.SetParameterAutoSend(1, true);
    }
}