﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour {

	[SerializeField]
	private Transform handTransform;
	
	[SyncVar]
	public GameObject CurrentObject;

	public GameObject CurrentStation;

	[SerializeField]
	private PlayerAnimatorController animatorController;
	[SerializeField]
	private Rigidbody rigidBody;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(!isLocalPlayer)
		{
			return ;
		}

		InputHandle();
	}

	public Transform GetHandTransform()
	{
		return handTransform;
	}

	public void SetCurrentObject(GameObject obj)
	{
		CurrentObject = obj;
	}

	private void InputHandle()
	{
		var xInput = Input.GetAxis("Horizontal") * 150.0f * Time.deltaTime;
		var zInput = Input.GetAxis("Vertical");

		var movementSpeed = Vector3.forward * 4.0f * zInput;

		this.transform.Rotate(transform.up * xInput );
		rigidBody.velocity = transform.TransformDirection(movementSpeed);

		
		animatorController.CmdSetRunAnimationParameter(zInput);

		if(Input.GetKeyDown(KeyCode.Space))
		{
			if(CurrentStation != null)
			{
				var station = CurrentStation.GetComponent<StationBase>();

				if (CurrentObject == null)
				{
					CmdTakeIngredient();
				}
				else
				{
					CmdPutIngredient();
				}
			}
		 }
	}

	void OnTriggerEnter(Collider obj)
	{
		if(obj.CompareTag("Station"))
		{
			CurrentStation = obj.gameObject;
			CurrentStation.GetComponent<StationView>().EnterSelectedMode();
		}
	}

	void OnTriggerExit(Collider obj)
	{
		if(CurrentStation == obj.gameObject)
		{
			CurrentStation.GetComponent<StationView>().ExitSelectedMode();
			CurrentStation = null;
		}
	}

	[Command]
	private void CmdTakeIngredient()
	{
		var stationBase = CurrentStation.GetComponent<StationBase>();

		stationBase.CmdTakeIngredient(this.gameObject);
		
		if(this.CurrentObject != null)
			animatorController.RpcSetHoldAnimation(true);
	}

	[Command]
	private void CmdPutIngredient()
	{
		var station = CurrentStation.GetComponent<StationBase>();

		if (station.stationType != StationEnum.INGREDIENT)
		{
			station.CmdPutItem(CurrentObject);
			animatorController.RpcSetHoldAnimation(false);
			CurrentObject = null;
		}
	}
}
