﻿using UnityEngine;
using UnityEngine.Networking;

public class StationController : StationBase {

	public GameObject prefab;
	
	[Command]
	public override void CmdTakeIngredient(GameObject player)
	{
		var playerController = player.GetComponent<PlayerController>();
		var prefab = this.GetComponent<StationController>().prefab;
		var instance = Instantiate(prefab, playerController.GetHandTransform());

		NetworkServer.Spawn(instance);

		playerController.CurrentObject = instance;
	}
}
