﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class StationView : MonoBehaviour
{
	[SerializeField]
	private MeshRenderer renderer;
	
	private Color selectedColor = new Color(0.1f, 0.1f, 0.1f, 1.0f);
	private Color defaultColor = Color.white;
	
	private Tween tween;

	void Awake()
	{
		
	}
	
	public void EnterSelectedMode()
	{
		renderer.material.color = selectedColor;
	}

	public void ExitSelectedMode()
	{
		renderer.material.color = defaultColor;
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
