﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TableController : StationBase {
	[SyncVar]
	public GameObject CurrentObject;
	public Transform PlateTransform;

	[Command]
	public override void CmdPutItem(GameObject item)
	{
		if(CurrentObject == null)
		{
			CurrentObject = item;
			CurrentObject.transform.position = PlateTransform.position;
			CurrentObject.transform.SetParent(null);
		}
	}
	
	[Command]
	public override void CmdTakeIngredient(GameObject player)
	{
		if(CurrentObject != null)
		{
			var playerController = player.GetComponent<PlayerController>();
			
			CurrentObject.transform.SetParent(playerController.GetHandTransform());
		
			TweenCurrentObject(CurrentObject, playerController.GetHandTransform().position);

			playerController.CurrentObject = CurrentObject;
			CurrentObject = null;
		}
	}
}
