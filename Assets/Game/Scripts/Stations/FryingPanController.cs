﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class FryingPanController : StationBase {
	
	[SyncVar]
	public bool IsBusy;
	[SyncVar]
	public bool IsReady;
	[SyncVar]
	public GameObject CurrentObject;
	public Transform PlateTransform;
	
	public GameObject TimerPrefab;
	
	[Command]
	public override void CmdPutItem(GameObject item)
	{
		Debug.Log("Tentando colocar");
		if(CurrentObject == null)
		{
			CurrentObject = item;
			CurrentObject.transform.position = PlateTransform.position;
			CurrentObject.transform.SetParent(null);

			InitializeTimer();
			
			CurrentObject.GetComponent<IngredientController>().RpcSetTrigger("Prepare");
		}
	}
	
	[Command]
	public override void CmdTakeIngredient(GameObject player)
	{
		Debug.Log("ARGH");

		if(!IsBusy && IsReady)
		{
			var playerController = player.GetComponent<PlayerController>();
			//Criar metodo que faça essas duas coisas, lá no playerController
			CurrentObject.transform.SetParent(playerController.GetHandTransform());
			CurrentObject.transform.position = playerController.GetHandTransform().position;

			playerController.CurrentObject = CurrentObject;
			CurrentObject = null;
		}
	}

	private void InitializeTimer()
	{
		IsBusy = true;

		var timerInstance = Instantiate(TimerPrefab, PlateTransform);

		var timerController = timerInstance.GetComponent<TimerController>();

		timerController.OnFinishTimer += () => {
			IsBusy = false;
			IsReady = true;
			
			CurrentObject.GetComponent<IngredientController>().RpcSetTrigger("Finish");
		};

		if(isServer)
			NetworkServer.Spawn(timerInstance);
	}
}
