﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Networking;

public class StationBase : NetworkBehaviour {

	public StationEnum stationType;

	protected Tween moveTween;

	public virtual void CmdTakeIngredient(GameObject player)
	{
	}
	
	public virtual void CmdPutItem(GameObject item)
	{
	}

	protected void TweenCurrentObject(GameObject obj, Vector3 to)
	{
		
	}
}
