﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TrashController : StationBase {
	
	[Command]
	public override void CmdPutItem(GameObject item)
	{
		NetworkServer.Destroy(item);
		base.CmdPutItem(item);
	}
}
