﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DeliveryTable : StationBase {

	[SyncVar]
	public GameObject CurrentObject;

	[SyncVar]
	public bool IsBusy;

	public Transform StandPoint;

	public Transform PlateTransform;

	[SyncVar(hook = "OnChangeCustomer")]
	public GameObject CurrentCustomer;

	private CustomerController customerController;

	public IngredientController GetCurrentObjectIngredientController()
	{
		return CurrentObject.GetComponent<IngredientController>();
	}

	[Command]
	public override void CmdPutItem(GameObject item)
	{
		Debug.Log("Tentando colocar na mesinha");
		if(CurrentObject == null)
		{
			CurrentObject = item;
			CurrentObject.transform.position = PlateTransform.position;
			CurrentObject.transform.SetParent(null);

			if (customerController != null)
			{
				customerController.ShouldConsume();
				Debug.Log("Consume ai!");
			}
		}
	}
	
	[Command]
	public override void CmdTakeIngredient(GameObject player)
	{
		var playerController = player.GetComponent<PlayerController>();
		//Criar metodo que faça essas duas coisas, lá no playerController
		CurrentObject.transform.SetParent(playerController.GetHandTransform());
		CurrentObject.transform.position = playerController.GetHandTransform().position;

		playerController.CurrentObject = CurrentObject;
		CurrentObject = null;
	}

	[Command]
	public void CmdConsumeDelivery()
	{
		NetworkServer.Destroy(CurrentObject);
		CurrentObject = null;
	}

	private void OnChangeCustomer(GameObject obj)
	{
		this.customerController = obj.GetComponent<CustomerController>();
	}
}
