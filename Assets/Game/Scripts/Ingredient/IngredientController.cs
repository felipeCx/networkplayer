﻿using UnityEngine;
using UnityEngine.Networking;

public class IngredientController : NetworkBehaviour
{
    public IngredientStateEnum IngredientStateType;
    public IntredientEnum IngredientType;
    public Animator stateAnimator;
    public ParticleSystem particle;

    [Command]
    public void CmdInstantiateParticle()
    {
        var particleInstance = Instantiate(particle, this.transform.position, this.transform.rotation);

        NetworkServer.Spawn(particleInstance.gameObject);
    }

    [ClientRpc]
    public void RpcSetTrigger(string trigger)
    {
        stateAnimator.SetTrigger(trigger);
    }
}