﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngredientState : StateMachineBehaviour {

	private IngredientController ingredientController;
	private MeshFilter meshFilter;
	public Mesh mesh;
	public Material material;

	public bool EnableParticle;
	public ParticleSystem Particle;

	public IngredientStateEnum state;

	void Awake()
	{
		
	}

	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		ingredientController = animator.GetComponent<IngredientController>();
		ingredientController.IngredientStateType = state;

		animator.GetComponent<MeshFilter>().mesh = mesh;

		if(EnableParticle)
		{
			ingredientController.CmdInstantiateParticle();
		}
	}

	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
    }
}
