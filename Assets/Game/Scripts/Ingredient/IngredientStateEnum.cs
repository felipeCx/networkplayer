﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum IngredientStateEnum {
	RAW,
	PREPARING,
	DONE
}
