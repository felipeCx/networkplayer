﻿using UnityEngine;
using UnityEngine.Networking;

public class MatchManager : NetworkBehaviour
{
    public static MatchManager Instance;

    [SerializeField] private GameObject gameOverPopupPrefab;

    [SerializeField] private CustomerSpawner spawner;
    private int customersAmount;
    private int currentRecord;

    [SyncVar] public int currentFinishedClients = 0;

    [SyncVar(hook = "OnChangeScore")] public int Score;

    private int connectedPlayers = 0;

    private bool isGameRunning;

    // Use this for initialization
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        customersAmount = spawner.GetCustomersAmount();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isGameRunning)
        {
            if (NetworkServer.connections.Count >= 2)
            {
                spawner.StartEssaCoisa();
                isGameRunning = true;
            }
        }
    }

    public override void OnStartServer()
    {
    }

    private void OnPlayerDisconnected(NetworkPlayer player)
    {
        connectedPlayers--;
    }

    public void StartGame()
    {
    }

    public void UpdateScore(int value)
    {
        Score += value;
    }

    void OnChangeScore(int score)
    {
        Score = score;
        UpdateHUD();
    }

    public void UpdateHUD()
    {
        HUDManager.Instance.UpdateScoreField(Score);
    }


    public void ShowGameOver()
    {
    }

    public void FinalizeCustomer()
    {
        currentFinishedClients++;

        if (currentFinishedClients >= customersAmount)
        {
            FinishGame();
        }
    }

    private void FinishGame()
    {
        var popup = Instantiate(gameOverPopupPrefab, this.transform.position, this.transform.rotation);

        NetworkServer.Spawn(popup);

        CheckRecord();

        popup.GetComponent<GameOverPopup>().FillPopup(Score, currentRecord);
    }

    private void CheckRecord()
    {
        currentRecord = PlayerPrefs.GetInt(PrefsConstants.RECORD_KEY, 0);

        if (this.Score > currentRecord)
        {
            UpdateRecord();
        }
    }

    private void UpdateRecord()
    {
        PlayerPrefs.SetInt(PrefsConstants.RECORD_KEY, this.Score);
        currentRecord = this.Score;
    }
}