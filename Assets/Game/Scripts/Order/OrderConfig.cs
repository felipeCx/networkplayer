﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="Game/OrderConfig")]
public class OrderConfig : ScriptableObject
{

	public int ingredientId;
	public Sprite icon;
	public Color color;
	public IntredientEnum ingredient;
}
