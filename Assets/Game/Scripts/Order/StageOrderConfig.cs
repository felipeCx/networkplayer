﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="Game/StageOrderConfig")]
public class StageOrderConfig : ScriptableObject {

	public List<OrderConfig> listConfig;
}
