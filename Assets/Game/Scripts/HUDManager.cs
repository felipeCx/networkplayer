﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class HUDManager : NetworkBehaviour {

	public static HUDManager Instance;
	public GameObject g;

	public Text txtScore;
	// Use this for initialization
	void Awake () {
		if(Instance == null)
		{
			Instance = this;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.E))
		{
			if(isServer)
			{

			var e = Instantiate(g);
			NetworkServer.Spawn(e);
			}
		}
	}

	public void UpdateScoreField(int value)
	{
		if(!isServer)
		{
			return;
		}

		RpcUpdateTxtScore(value);
	}

	[ClientRpc]
	private void RpcUpdateTxtScore(int value)
	{
		txtScore.text = value.ToString();
	}
}
